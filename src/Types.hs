{-# LANGUAGE PatternSynonyms #-}

module Types
    ( Options(Options)
    , Command(..)
    , CacheDir
    , CacheFile
    , DownloadOptions(DownloadOptions)
    , PickerOptions(PickerOptions)
    , pattern DownloadCommand
    , pattern PickerCommand
    ) where

import           GHC.Generics ( Generic )

import           Path         ( Abs, Dir, File, Path )

pattern DownloadCommand :: DownloadOptions -> Options
pattern DownloadCommand options = Options (Download options)

pattern PickerCommand :: PickerOptions -> Options
pattern PickerCommand options = Options (Picker options)

data Options = Options { command :: Command }
    deriving ( Show, Generic )

data Command = Picker PickerOptions | Download DownloadOptions
    deriving ( Show, Generic )

data PickerOptions = PickerOptions { noPrintSelection :: Bool }
    deriving ( Generic, Show )

data DownloadOptions = DownloadOptions { quiet :: Bool }
    deriving ( Generic, Show )

type CacheDir = Path Abs Dir

type CacheFile = Path Abs File
