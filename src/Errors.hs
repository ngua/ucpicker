module Errors
    ( Error(..)
    , mkParseError
    , fromPeekException
    , mkCacheError
    , fromIOError
    , mkHttpError
    ) where

import           Control.Exception    ( Exception )

import           Data.Generics.Labels ()
import           Data.Store           ( PeekException, peekExMessage )
import           Data.Text            ( Text )
import qualified Data.Text            as T

import           GHC.Generics         ( Generic )

import           TextShow             ( TextShow(showt) )

data Error = HTTP HTTPError | Parse ParseError | Cache CacheError
    deriving ( Generic )

newtype ParseError = ParseError Text
    deriving ( Show, Generic )

newtype CacheError = CacheError Text
    deriving ( Show, Generic )

newtype HTTPError = HTTPError Text
    deriving ( Show, Generic )

instance Show Error where
    show e@(Parse (ParseError t)) = unwrapText e t
    show e@(Cache (CacheError t)) = unwrapText e t
    show e@(HTTP (HTTPError t))   = unwrapText e t

instance Exception Error

unwrapText :: Error -> Text -> String
unwrapText e t = T.unpack $ prefix e <> t

prefix :: Error -> Text
prefix (Parse _) = "Parse error: "
prefix (Cache _) = "Cache error: "
prefix _         = mempty

mkParseError :: Show a => a -> Error
mkParseError = Parse . ParseError . T.pack . show

mkCacheError :: (a -> CacheError) -> a -> Error
mkCacheError g e = Cache $ g e

fromPeekException :: PeekException -> CacheError
fromPeekException = CacheError . peekExMessage

fromIOError :: IOError -> CacheError
fromIOError = CacheError . showt

mkHttpError :: Show a => Maybe a -> Error
mkHttpError (Just e) = HTTP . HTTPError . T.pack . show $ e
mkHttpError _        = HTTP . HTTPError $ "Encountered unknown network problem"
