{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TypeApplications #-}

module Search
    ( byPoint
    , byName
    , mkByName
    , ByName
    , ByPoint
    , Search
    , mkPointSearch
    , mkNameSearch
    ) where

import           Control.Monad.Trans.State.Strict

import           Data.Generics.Product            ( getField )
import qualified Data.HashMap.Strict              as HM
import           Data.Hashable                    ( Hashable )
import           Data.Ord                         ( Down(Down), comparing )
import           Data.Ratio                       ( (%), Ratio )
import qualified Data.Text                        as T
import           Data.Text                        ( Text )
import           Data.Text.Metrics                ( levenshteinNorm )
import qualified Data.Vector                      as V
import           Data.Vector                      ( Vector )
import qualified Data.Vector.Algorithms.Intro     as V

import           GHC.Generics                     ( Generic )

import           Lens.Micro

import           Text.Read                        ( readMaybe )

import           UCD
import           UCD.Types                        ( mkPoint )

data ByName = ByName
    { matches :: Vector CodePoint
    , cache   :: HM.HashMap (Search Text) (Vector CodePoint)
    }
    deriving ( Show, Generic )

type ByPoint = HM.HashMap Point CodePoint

newtype Search a = Search { unSearch :: a }
    deriving ( Show, Generic, Eq, Hashable )

mkNameSearch :: Text -> Maybe (Search Text)
mkNameSearch t
    | T.null t = Nothing
    | T.length t < 2 = Nothing
    | otherwise = Just $ Search t

mkPointSearch :: Text -> Maybe (Search Point)
mkPointSearch t = Search <$> (mkPoint =<< readMaybe (T.unpack t))

byName :: Search Text -> State ByName (Vector CodePoint)
byName search = get >>= \current -> case HM.lookup search (current ^. #cache) of
    Just m  -> do
        put $ current & #matches .~ m
        return m
    Nothing -> do
        let !m = matchNames 0.5 (current ^. #matches) (search ^. #unSearch)
        put $ current & (#cache %~ HM.insert search m) . (#matches .~ m)
        return m

byPoint :: ByPoint -> Search Point -> Maybe CodePoint
byPoint ucm n = HM.lookup (n ^. #unSearch) ucm

matchNames :: Ratio Int -> Vector CodePoint -> Text -> Vector CodePoint
matchNames eps ucv search =
    V.modify (V.sortBy (comparing byMatch)) $ matches' ucv
  where
    byMatch    = Down . getField @"match"

    matchName' = matchName eps

    matches'   = V.mapMaybe (`matchName'` search)

matchName :: Ratio Int -> CodePoint -> Text -> Maybe CodePoint
matchName _ _ search
    | T.null search = Nothing
matchName eps point search =
    if total < eps then Nothing else Just $ point & #match .~ total
  where
    [ n, s ] = T.toLower <$> [ point ^. #name . #unName, search ]

    total    = bonus match

    match    = levenshteinNorm n s

    isInfix  = T.isInfixOf s n

    bonus m
        | m == 0 % 1 = m
        | isInfix && m < eps = m + eps
        | isInfix && m < 3 % 5 = minimum [ 3 % 5, m + 1 % 4 ]
        | otherwise = m

mkByName :: Vector CodePoint -> ByName
mkByName matches = ByName { matches, cache = mempty }
