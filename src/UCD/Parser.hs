{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}

module UCD.Parser ( unicodeData ) where

import           Control.Applicative              ( Alternative((<|>)), many )
import           Control.Monad                    ( MonadPlus(mzero) )

import           Data.Attoparsec.ByteString.Char8 hiding ( Number, number )
import qualified Data.ByteString.Char8            as C
import           Data.Generics.Product

import           Errors

import           Lens.Micro

import           Prelude                          hiding ( takeWhile )

import           UCD.Types

delim :: Parser Char
delim = char ';'

unicodeData :: C.ByteString -> Either Error UnicodeList
unicodeData c = parseOnly controlOrPoint c
    <&> filter (filterControl . getField @"category")
    & _Left %~ mkParseError
  where
    filterControl :: Category -> Bool
    filterControl (Other _) = False
    filterControl _         = True

controlOrPoint :: Parser UnicodeList
controlOrPoint = many $ (controlCodePoint <|> codePoint) <* endOfLine

codePoint :: Parser CodePoint
codePoint = do
    p <- point
    _ <- delim
    n <- name
    _ <- delim
    c <- category
    goToEnd
    return $ mkCodePoint p n c

controlCodePoint :: Parser CodePoint
controlCodePoint = do
    p <- point
    _ <- delim >> controlName >> delim
    c <- category
    _ <- delim
        >> digit
        >> delim
        >> takeWhile (`elem` [ 'B', 'S', 'N', 'W', 'L' ])
        >> count 5 delim
        >> char 'N'
        >> delim
    n <- name
    goToEnd
    return $ mkCodePoint p n c

name :: Parser Name
name = takeWhile (';' /=) >>= \n -> return (mkName n)

point :: Parser Point
point = hexadecimal @Int >>= \i -> maybe mzero return (mkPoint i)

category :: Parser Category
category = peekChar
    >>= \case
        Just 'C' -> Other <$> other
        Just 'L' -> Letter <$> letter
        Just 'M' -> Mark <$> mark
        Just 'N' -> Number <$> number
        Just 'P' -> Punctuation <$> punctuation
        Just 'S' -> Symbol <$> symbol
        Just 'Z' -> Separator <$> separator
        _        -> mzero

goToEnd :: Parser ()
goToEnd = skipWhile ('\n' /=)

controlName :: Parser ()
controlName = char '<' >> takeWhile ('>' /=) >> char '>' >> pure ()

number :: Parser Number
number = char 'N'
    >> (char 'd' >> pure Decimal)
    <|> (char 'l' >> pure LetterN)
    <|> (char 'o' >> pure OtherN)

other :: Parser Other
other = char 'C'
    >> (char 'c' >> pure Control)
    <|> (char 'f' >> pure Format)
    <|> (char 'n' >> pure NotAssigned)
    <|> (char 'o' >> pure Private)
    <|> (char 's' >> pure Surrogate)

letter :: Parser Letter
letter = char 'L'
    >> (char 'l' >> pure Lowercase)
    <|> (char 'm' >> pure ModifierL)
    <|> (char 'o' >> pure OtherL)
    <|> (char 't' >> pure Titlecase)
    <|> (char 'u' >> pure Uppercase)

mark :: Parser Mark
mark = char 'M'
    >> (char 'c' >> pure Spacing)
    <|> (char 'e' >> pure Enclosing)
    <|> (char 'n' >> pure Nonspacing)

separator :: Parser Separator
separator = char 'Z'
    >> (char 'l' >> pure Line)
    <|> (char 'p' >> pure Paragraph)
    <|> (char 's' >> pure Space)

symbol :: Parser Symbol
symbol = char 'S'
    >> (char 'c' >> pure Currency)
    <|> (char 'k' >> pure ModifierS)
    <|> (char 'm' >> pure Math)
    <|> (char 'o' >> pure OtherS)

punctuation :: Parser Punctuation
punctuation = char 'P'
    >> (char 'c' >> pure Connector)
    <|> (char 'd' >> pure Dash)
    <|> (char 'e' >> pure Close)
    <|> (char 'i' >> pure Initial)
    <|> (char 'f' >> pure Final)
    <|> (char 's' >> pure Open)
    <|> (char 'o' >> pure OtherP)
