{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeApplications #-}

module UCD.Share ( readCache, writeCacheQuiet, writeCacheLoud ) where

import           Control.Concurrent         ( threadDelay )
import           Control.Concurrent.Async
import           Control.Exception          ( try )
import           Control.Monad.IO.Class     ( liftIO )
import           Control.Monad.Trans.Except ( ExceptT(ExceptT), runExceptT )

import qualified Data.ByteString.Char8      as C
import           Data.ByteString.Char8      ( ByteString )
import           Data.Foldable              ( traverse_ )
import           Data.Store                 ( decode, encode )
import qualified Data.Text                  as T
import qualified Data.Text.IO               as T

import qualified Errors                     as Err
import           Errors                     ( Error, mkHttpError )

import           Lens.Micro

import           Network.HTTP.Simple

import           Path

import           System.IO                  ( stderr )

import           Text.URI                   ( URI, renderStr )
import           Text.URI.QQ                ( uri )

import qualified UCD.Parser                 as Parser
import           UCD.Types

readCache :: Path a File -> ExceptT Error IO UnicodeData
readCache fp = do
    cacheFile <- ExceptT . liftIO $ do
        f <- try @IOError $ C.readFile (toFilePath fp)
        return $ f & _Left %~ Err.mkCacheError Err.fromIOError
    ExceptT . return $ decode cacheFile
        & _Left %~ Err.mkCacheError Err.fromPeekException
        & _Right %~ mkUnicodeData

writeCacheQuiet :: Path a File -> IO (Either Error UnicodeData)
writeCacheQuiet fp = runExceptT $ do
    raw <- parseUCDResponse
    _ <- ExceptT . liftIO $ do
        w <- try . C.writeFile (toFilePath fp) . encode $ raw
        return $ w & _Left %~ Err.mkCacheError Err.fromIOError
    ExceptT . return . Right . mkUnicodeData $ raw

writeCacheLoud :: Path a File -> IO (Either Error UnicodeData)
writeCacheLoud fp = do
    a <- async . runExceptT $ parseUCDResponse
    printStdErr "Fetching UnicodeData.txt ...  "
    runExceptT $ do
        raw <- ExceptT $ loop a
        liftIO . printStdErr $ "Parsing UnicodeData.txt ...  "
        ExceptT . liftIO $ do
            w <- try . C.writeFile (toFilePath fp) $ encode raw
            return $ w & _Left %~ Err.mkCacheError Err.fromIOError
        ExceptT . return . Right . mkUnicodeData $ raw
  where
    loop a = poll a
        >>= \p -> do
            case p of
                Nothing          -> do
                    traverse_ (\c -> threadDelay 100000 >> T.hPutStr stderr c) $ (`T.snoc` '\r')
                        <$> [ " /", " -", " \\", " |" ]
                    loop a
                Just (Right res) -> do
                    case res of
                        Right r -> printStdErr "Done" >> (return . Right $ r)
                        Left e  -> return . Left . mkHttpError . Just $ show e
                Just (Left e)    -> return . Left . mkHttpError . Just $ show e
    printStdErr = T.hPutStrLn stderr

parseUCDResponse :: ExceptT Error IO UnicodeList
parseUCDResponse =
    getData ucdAddress >>= \r -> ExceptT . return . Parser.unicodeData $ r

ucdAddress :: URI
ucdAddress = [uri|https://unicode.org/Public/UCD/latest/ucd/UnicodeData.txt|]

getData :: URI -> ExceptT Error IO ByteString
getData u = ExceptT $ do
    r <- liftIO . try @HttpException . httpBS . uriToRequest $ u
    case r <&> getResponseStatusCode of
        Right 200 -> return $ r <&> getResponseBody
            & _Left .~ mkHttpError (r ^? _Left <&> show)
        _         -> return . Left $ mkHttpError (r ^? _Left <&> show)

uriToRequest :: URI -> Request
uriToRequest = parseRequest_ . renderStr
