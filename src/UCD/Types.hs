{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TypeApplications #-}

module UCD.Types
    ( CodePoint(CodePoint)
    , Name
    , Printable
    , Point(Point)
    , Category(..)
    , Letter(..)
    , Mark(..)
    , Number(..)
    , Punctuation(..)
    , Symbol(..)
    , Separator(..)
    , Other(..)
    , mkName
    , mkPoint
    , mkCodePoint
    , UnicodeList
    , UnicodeData
    , UnicodeMap
    , mkUnicodeData
    , UnicodeVec
    , mkUnicodeMap
    , mkPrintable
    ) where

import           Data.ByteString.Char8 ( ByteString )
import           Data.Char             ( ord )
import           Data.Generics.Labels  ()
import qualified Data.HashMap.Strict   as HM
import           Data.Hashable
import           Data.Ix               ( Ix(inRange) )
import           Data.Ratio            ( (%), Ratio )
import           Data.Store            ( Store )
import           Data.String           ( IsString )
import           Data.Text             ( Text, toTitle )
import qualified Data.Text             as T
import           Data.Text.Encoding    ( decodeUtf8 )
import           Data.Vector           ( Vector )
import qualified Data.Vector           as V

import           GHC.Generics          ( Generic )

import           Lens.Micro
import           Lens.Micro.Platform   ( packed )

import           Numeric               ( showHex )

import           TextShow              ( TextShow(..), fromText )

data UnicodeData = UnicodeData { byPoint :: UnicodeMap, byName :: UnicodeVec }
    deriving ( Show, Generic, Eq )

type UnicodeList = [CodePoint]

type UnicodeMap = HM.HashMap Point CodePoint

type UnicodeVec = Vector CodePoint

data CodePoint = CodePoint
    { point    :: Point
    , name     :: Name
    , category :: Category
    , match    :: Ratio Int
    }
    deriving ( Show, Generic, Eq, Hashable, Store )

newtype Name = Name { unName :: Text }
    deriving newtype ( Show, Eq, Hashable, IsString, Store )
    deriving stock Generic

newtype Point = Point Int
    deriving newtype ( Show, Eq, Ord, Num, Hashable, Store )
    deriving stock Generic

data Printable = Printable { name :: Name, asPoint :: Point, asChar :: Char }
    deriving ( Show, Generic )

instance TextShow Point where
    showb (Point n) = fromText $ "U+"
        <> (showHex n "" ^. packed & T.justifyRight 4 '0')

data Category
    = Letter Letter
    | Mark Mark
    | Number Number
    | Other Other
    | Punctuation Punctuation
    | Separator Separator
    | Symbol Symbol
    deriving ( Show, Generic, Eq, Ord, Hashable, Store )

data Letter = Lowercase | ModifierL | OtherL | Titlecase | Uppercase
    deriving ( Show, Generic, Eq, Ord, Hashable, Store )

data Mark = Spacing | Enclosing | Nonspacing
    deriving ( Show, Generic, Eq, Ord, Hashable, Store )

data Number = Decimal | LetterN | OtherN
    deriving ( Show, Generic, Eq, Ord, Hashable, Store )

data Punctuation = Connector | Dash | Close | Final | Initial | OtherP | Open
    deriving ( Show, Generic, Eq, Ord, Hashable, Store )

data Symbol = Currency | ModifierS | Math | OtherS
    deriving ( Show, Generic, Eq, Ord, Hashable, Store )

data Separator = Line | Paragraph | Space
    deriving ( Show, Generic, Eq, Ord, Hashable, Store )

data Other = Control | Format | NotAssigned | Private | Surrogate
    deriving ( Show, Generic, Eq, Ord, Hashable, Store )

charMaxValue :: Int
charMaxValue = ord $ maxBound @Char

mkCodePoint :: Point -> Name -> Category -> CodePoint
mkCodePoint point name category =
    CodePoint { point, name, category, match = 0 % 1 }

mkName :: ByteString -> Name
mkName c = Name { unName }
  where
    unName = toTitle . decodeUtf8 $ c

mkPoint :: Int -> Maybe Point
mkPoint n
    | inRange (0, charMaxValue) n = Just $ Point n
    | otherwise = Nothing

mkUnicodeData :: UnicodeList -> UnicodeData
mkUnicodeData ucr =
    UnicodeData { byPoint = mkUnicodeMap ucr, byName = mkUnicodeVec ucr }

mkUnicodeMap :: UnicodeList -> UnicodeMap
mkUnicodeMap cs = HM.fromList [ (c ^. #point, c) | c <- cs ]

mkUnicodeVec :: UnicodeList -> UnicodeVec
mkUnicodeVec = V.fromList

mkPrintable :: Name -> Point -> Char -> Printable
mkPrintable = Printable
