{-# LANGUAGE OverloadedLabels #-}

module UI ( app ) where

import           Brick                      ( (<+>), (<=>) )
import qualified Brick                      as B
import qualified Brick.Focus                as B
import qualified Brick.Widgets.Border       as B
import qualified Brick.Widgets.Border.Style as B
import qualified Brick.Widgets.Edit         as B
import qualified Brick.Widgets.List         as B

import qualified Data.Text                  as T

import qualified Graphics.Vty               as Vty

import           Lens.Micro

import qualified UI.ByName                  as ByName
import qualified UI.ByPoint                 as ByPoint
import qualified UI.Results                 as Results
import           UI.Types
import           UI.Utils                   ( dimAttr, footerAttr, titleAttr )

app :: B.App State e Name
app = B.App
    { B.appDraw         = drawUI
    , B.appChooseCursor = B.showFirstCursor
    , B.appHandleEvent  = handleEvent
    , B.appStartEvent   = pure
    , B.appAttrMap      = const theMap
    }

handleEvent :: State -> B.BrickEvent Name e -> B.EventM Name (B.Next State)
handleEvent st ev@(B.VtyEvent (Vty.EvKey k m)) = case (k, m) of
    (Vty.KEsc, []) -> B.halt st

    (Vty.KChar 'c', [ Vty.MCtrl ]) -> B.halt st

    _ -> case B.focusGetCurrent $ st ^. #focus of
        Just SearchByName  -> ByName.handleEvent st ev
        Just SearchByPoint -> ByPoint.handleEvent st ev
        Just ResultList    -> Results.handleEvent st ev
        Nothing            -> B.continue st

handleEvent st _ = B.continue st

drawUI :: State -> [B.Widget Name]
drawUI st = [ B.padAll 1 contents ]
  where
    contents    =
        B.withBorderStyle B.unicode $ (B.borderWithLabel $ B.txt "Search") searchArea
        <=> B.padTop (B.Pad 1) resultsArea
        <=> B.vLimit 1 (B.withAttr footerAttr (footerArea <+> B.fill ' '))

    searchArea  = ByName.drawUI st <=> ByPoint.drawUI st

    resultsArea = Results.drawUI st

    footerArea  = drawFooter st

drawFooter :: State -> B.Widget Name
drawFooter st = defaultFooter : footer & T.intercalate "  " & B.txt
  where
    footer        = case B.focusGetCurrent $ st ^. #focus of
        Just ResultList    -> Results.footer
        Just SearchByName  -> ByName.footer
        Just SearchByPoint -> ByPoint.footer
        Nothing            -> mempty

    defaultFooter = "ESC/^C - Quit"

theMap :: B.AttrMap
theMap =
    B.attrMap Vty.defAttr
              [ (B.editAttr, defaultAttr)
              , (B.editFocusedAttr, defaultAttr `Vty.withStyle` Vty.underline)
              , (B.listAttr, defaultAttr)
              , ( B.listSelectedAttr
                    , defaultAttr `Vty.withStyle` Vty.reverseVideo
                    )
              , (titleAttr, defaultAttr `Vty.withStyle` Vty.bold)
              , (dimAttr, defaultAttr `Vty.withStyle` Vty.dim)
              , (footerAttr, defaultAttr `Vty.withStyle` Vty.reverseVideo)
              ]
  where
    defaultAttr = B.fg Vty.white
