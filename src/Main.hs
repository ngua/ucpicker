module Main where

import           Command

import           Options.Applicative

main :: IO ()
main = run =<< execParser opts'
  where
    opts' = info (opts <**> helper) $ fullDesc
        <> progDesc "Download and interactively pick chars from full unicode db"
