{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeApplications #-}

module Command ( run, opts ) where

import qualified Brick                        as B

import           Control.Exception            ( try )
import           Control.Exception.Base       ( Exception(displayException) )
import           Control.Monad                ( unless )
import           Control.Monad.IO.Class       ( MonadIO(liftIO) )
import           Control.Monad.Trans.Except

import           Data.Generics.Product.Fields
import qualified Data.Text                    as T
import qualified Data.Text.IO                 as T
import qualified Data.Vector                  as V

import           Errors                       ( Error )
import qualified Errors                       as Err

import           Lens.Micro
import           Lens.Micro.Platform          ( packed )

import           Options.Applicative

import           Path
                 ( (</>), Dir, File, Rel, reldir, relfile )
import           Path.IO

import           System.IO                    ( hPutStrLn, stderr )

import           Types

import           UCD                          ( UnicodeData, pointToChar )
import           UCD.Share

import           UI                           ( app )
import           UI.Types                     ( newState )

run :: Options -> IO ()
run (DownloadCommand options) = download options
run (PickerCommand options)   = pick options
run (Options _)               = undefined

pick :: PickerOptions -> IO ()
pick options = do
    cached <- readCacheOrWrite
    either (hPutStrLn stderr . displayException)
           (launch (options ^. #noPrintSelection))
           cached
  where
    launch :: Bool -> UnicodeData -> IO ()
    launch noPrintOpt ucd = do
        finalState <- B.defaultMain app $ newState ucd
        unless noPrintOpt . printResults $ finalState ^. #selected

    printResults = T.putStrLn
        . T.unwords
        . V.toList
        . fmap (T.singleton . pointToChar . getField @"point")

download :: DownloadOptions -> IO ()
download options = do
    work <- runExceptT $ do
        cacheLocation <- getCacheLocation
        _ <- if options ^. #quiet
             then write writeCacheQuiet cacheLocation
             else write writeCacheLoud cacheLocation
        return cacheLocation
    either (hPutStrLn stderr . displayException)
           (T.hPutStrLn stderr . message)
           work
  where
    message f = "Wrote cachefile " <> show f ^. packed

    write g = ExceptT . liftIO . g

opts :: Parser Options
opts = Options <$> subCommand

subCommand :: Parser Command
subCommand = subparser $ downloader <> picker
  where
    downloader = command "download"
        . info (Download <$> downloadOpts <**> helper) $ progDesc "Download data from unicode.org and populate cache"

    picker     = command "pick"
        . info (Picker <$> pickerOpts <**> helper) $ progDesc "Search and pick unicode"

pickerOpts :: Parser PickerOptions
pickerOpts = PickerOptions
    <$> switch (long "no-print"
                <> short 'p'
                <> help "Don't print selection to stdout")

downloadOpts :: Parser DownloadOptions
downloadOpts = DownloadOptions
    <$> switch (long "quiet" <> short 'q' <> help "Suppress output to stderr")

cachefilePath :: CacheDir -> CacheFile
cachefilePath dir = dir </> [relfile|uc.data|]

cachedirPath :: IO CacheDir
cachedirPath = getXdgDir XdgCache (Just [reldir|ucpicker|])

getCacheLocation :: ExceptT Error IO CacheFile
getCacheLocation = do
    dir <- liftIO cachedirPath
    _ <- ensureCacheDir_ dir
    return $ cachefilePath dir

readCacheOrWrite :: IO (Either Error UnicodeData)
readCacheOrWrite = runExceptT $ do
    cacheLocation <- getCacheLocation
    cached <- liftIO . runExceptT . readCache $ cacheLocation
    either (\_ -> ExceptT $ writeCacheQuiet cacheLocation)
           (ExceptT . return . Right)
           cached

ensureCacheDir_ :: CacheDir -> ExceptT Error IO ()
ensureCacheDir_ dir = ExceptT $ (try @IOError $ createDirIfMissing False dir)
    >>= \o -> return $ o & _Left %~ Err.mkCacheError Err.fromIOError
