{-# LANGUAGE OverloadedLabels #-}

module UCD
    ( pointToChar
    , Point
    , Name
    , CodePoint
    , UnicodeData
      -- , UnicodeMap
    , Printable
    , writeCacheQuiet
    , writeCacheLoud
    , readCache
    , toPrintable
    ) where

import           Data.Char  ( chr )

import           Lens.Micro

import           UCD.Share
import           UCD.Types

pointToChar :: Point -> Char
pointToChar (Point n) = chr n

toPrintable :: CodePoint -> Printable
toPrintable cp = mkPrintable name point asChar
  where
    name   = cp ^. #name

    point  = cp ^. #point

    asChar = cp ^. #point & pointToChar
