{-# LANGUAGE OverloadedLabels #-}

module UI.ByPoint ( handleEvent, drawUI, footer ) where

import qualified Brick              as B
import           Brick              ( (<+>) )
import qualified Brick.Focus        as B
import qualified Brick.Widgets.Edit as B

import           Data.Text          ( Text )
import qualified Data.Vector        as V

import qualified Graphics.Vty       as Vty

import           Lens.Micro

import qualified Search
import           Search             ( mkPointSearch )

import           UI.Types
import           UI.Utils

doSearch :: State -> State
doSearch st = st
    & (#results .~ results) . (#resultsList .~ brickList ResultList results)
  where
    results   = maybe mempty V.singleton search

    getSearch = st ^. #pointSearchEditor & processSearch & mkPointSearch

    search    = Search.byPoint (st ^. #searchByPoint) =<< getSearch

handleEvent :: State -> B.BrickEvent Name e -> B.EventM Name (B.Next State)
handleEvent st ev = case ev of
    (B.VtyEvent v@(Vty.EvKey k _)) -> case k of
        Vty.KChar '\t' -> B.continue $ st & #focus %~ B.focusNext

        Vty.KBackTab   -> B.continue $ st & #focus %~ B.focusPrev

        Vty.KEnter     -> B.continue $ st & doSearch

        _              -> do
            r <- B.handleEditorEvent v $ st ^. #pointSearchEditor
            B.continue $ st & #pointSearchEditor .~ r

    _ -> B.continue st

drawUI :: State -> B.Widget Name
drawUI st = hTitle "By hex codepoint: "
    <+> editor SearchByPoint st (st ^. #pointSearchEditor)

footer :: [Text]
footer = [ "RET - Perform search" ]
