{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE ViewPatterns #-}

module UI.Results ( drawUI, handleEvent, footer ) where

import qualified Brick                      as B
import           Brick                      ( (<+>), (<=>) )
import qualified Brick.Focus                as B
import qualified Brick.Widgets.Border       as B
import qualified Brick.Widgets.Border.Style as B
import qualified Brick.Widgets.Center       as B
import qualified Brick.Widgets.Edit         as B
import qualified Brick.Widgets.List         as B

import           Control.Monad.IO.Class     ( MonadIO(liftIO) )

import           Data.Char                  ( digitToInt, isDigit )
import           Data.List.Extra            ( snoc )
import qualified Data.Text                  as T
import           Data.Text                  ( Text )
import           Data.Text.Zipper           ( clearZipper )
import qualified Data.Vector                as V
import           Data.Vector                ( Vector )

import qualified Graphics.Vty               as Vty

import           Lens.Micro

import           Search                     ( mkByName )

import           System.Clipboard           ( setClipboardString )

import           TextShow                   ( TextShow(showt) )

import           UCD
                 ( CodePoint, pointToChar, toPrintable )

import           UI.Types
                 ( Mode(Jump, Normal), Name(..), State )
import           UI.Utils                   ( concatDigits, dimAttr )

handleEvent :: State -> B.BrickEvent Name e -> B.EventM Name (B.Next State)
handleEvent st (B.VtyEvent v@(Vty.EvKey k _)) = case st ^. #mode of
    Jump j -> case k of
        Vty.KChar k' -> if
            | isDigit k' -> B.continue $ st
                & #mode .~ Jump (j `snoc` digitToInt k')
            | k' == 'g' ->
                let index = concatDigits j
                in
                    B.continue $ st
                    & (#resultsList %~ B.listMoveTo index) . (#mode .~ Normal)
            | otherwise -> B.continue $ st & #mode .~ Normal
        _            -> B.continue $ st & #mode .~ Normal

    Normal -> case k of
        Vty.KChar '\t' -> B.continue $ st & #focus %~ B.focusNext

        Vty.KBackTab   -> B.continue $ st & #focus %~ B.focusPrev

        Vty.KEnter     -> do
            B.continue $ st & selectcodePoint

        Vty.KBS        -> do
            B.continue $ st & #selected %~ deleteLastSelected

        Vty.KChar 's'  ->
            B.continue $ st & #focus %~ B.focusSetCurrent SearchByName

        Vty.KChar 'x'  -> B.continue $ st
            & clearAll
            & #focus %~ B.focusSetCurrent SearchByName

        Vty.KChar 'y'  -> do
            _ <- liftIO . setClipboardString $ st
                ^.. #selected . traverse . #point
                <&> (: []) . pointToChar
                & unwords
            B.continue st

        Vty.KChar k'   ->
            if isDigit k'
            then B.continue $ st & #mode .~ Jump [ digitToInt k' ]
            else defaultSt

        _              -> defaultSt
  where
    defaultSt = do
        r <- B.handleListEventVi B.handleListEvent v $ st ^. #resultsList
        B.continue $ st & (#resultsList .~ r)

handleEvent st _ = B.continue st

selectcodePoint :: State -> State
selectcodePoint st = st & #selected %~ \v' ->
    maybe v' (V.snoc v' . snd) selected
  where
    selected = st ^. #resultsList & B.listSelectedElement

clearAll :: State -> State
clearAll st = st
    & (#results .~ mempty)
    . (#resultsList %~ B.listClear)
    . (#searchByName .~ mkByName (st ^. #allCodePoints))
    . (#nameSearchEditor %~ B.applyEdit clearZipper)

deleteLastSelected :: Vector CodePoint -> Vector CodePoint
deleteLastSelected cs@(V.null -> True) = cs
deleteLastSelected cs                  = V.init cs

drawUI :: State -> B.Widget Name
drawUI st =
    B.padAll 1 $ B.renderListWithIndex renderPoint False (st ^. #resultsList)
    <=> selected
  where
    renderPoint i _ c = B.withAttr dimAttr (B.txt (showt i <> ": "))
        <+> B.hLimit 100 (printed c)

    selected          = B.withBorderStyle B.unicode B.hBorder
        <=> B.hCenterWith Nothing (B.vLimit 2 $ B.txt renderSelection)

    renderSelection   = st ^.. #selected . traverse . #point
        <&> T.singleton . pointToChar
        & T.unwords

    printed cp =
        let p      = toPrintable cp
            asChar = p ^. #asChar & T.singleton
            name   = p ^. #name . #unName
        in
            B.txt (asChar <> "   ")
            <+> B.txt (name <> " ")
            <+> B.withAttr dimAttr
                           (B.txt (mconcat [ "  "
                                           , "("
                                           , p ^. #asPoint & showt
                                           , ")"
                                           ]))

footer :: [Text]
footer = [ "RET - Select"
         , "s - Back to search"
         , "x - Clear results/search"
         , "<n>g - Jump to n"
         , "y - Yank selected"
         , "<Backspace> - Delete selected"
         ]
