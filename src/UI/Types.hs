{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE StrictData #-}

module UI.Types ( State, Name(..), Mode(..), newState ) where

import qualified Brick.Focus        as B
import qualified Brick.Widgets.Edit as B
import qualified Brick.Widgets.List as B

import           Data.Text          ( Text )
import           Data.Vector        ( Vector )

import           GHC.Generics       ( Generic )

import           Lens.Micro

import qualified Search

import           UCD                ( UnicodeData )
import           UCD.Types          ( CodePoint )

data Name = SearchByName | SearchByPoint | ResultList
    deriving ( Show, Generic, Eq, Ord )

data State = State
    { nameSearchEditor  :: B.Editor Text Name
    , pointSearchEditor :: B.Editor Text Name
    , focus             :: B.FocusRing Name
    , resultsList       :: B.List Name CodePoint
    , results           :: Vector CodePoint
    , selected          :: Vector CodePoint
    , searchByPoint     :: Search.ByPoint
    , searchByName      :: Search.ByName
    , allCodePoints     :: Vector CodePoint
    , mode              :: Mode
    }
    deriving ( Generic )

data Mode = Normal | Jump [Int]
    deriving ( Show, Generic )

newState :: UnicodeData -> State
newState ucd = State
    { nameSearchEditor  = B.editor SearchByName (Just 1) mempty
    , pointSearchEditor = B.editor SearchByPoint (Just 1) mempty
    , focus             =
          B.focusRing [ SearchByName, SearchByPoint, ResultList ]
    , resultsList       = B.list ResultList mempty 1
    , results           = mempty
    , selected          = mempty
    , searchByName      = Search.mkByName (ucd ^. #byName)
    , searchByPoint     = ucd ^. #byPoint
    , allCodePoints     = ucd ^. #byName
    , mode              = Normal
    }
