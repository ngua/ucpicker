{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeApplications #-}

module UI.ByName ( handleEvent, drawUI, footer ) where

import qualified Brick                            as B
import           Brick                            ( (<+>) )
import qualified Brick.Focus                      as B
import qualified Brick.Widgets.Edit               as B
import qualified Brick.Widgets.List               as B

import           Control.Monad.Trans.State.Strict ( runState )

import           Data.Generics.Product            ( getField )
import           Data.Text                        ( Text )

import qualified Graphics.Vty                     as Vty

import           Lens.Micro

import qualified Search
import           Search
                 ( Search, mkByName, mkNameSearch )

import           UI.Types
import           UI.Utils

doSearch :: State -> Search Text -> State
doSearch st search = let (res, next) = results search
                     in
                         st
                         & (#results .~ res)
                         . (#searchByName .~ next)
                         . (#resultsList .~ brickList ResultList res)
  where
    results t = runState (t & Search.byName) (st ^. #searchByName)

handleEntry :: State -> State
handleEntry st = maybe st (doSearch st) (getSearch st)

handleBackSpace :: State -> State
handleBackSpace st = maybe clearCache (doSearch st) (getSearch st)
  where
    clearCache = st
        & (#searchByName .~ mkByName (st ^. #allCodePoints))
        . (#results .~ mempty)
        . (#resultsList %~ B.listClear)

getSearch :: State -> Maybe (Search Text)
getSearch = mkNameSearch . processSearch . getField @"nameSearchEditor"

handleEvent :: State -> B.BrickEvent Name e -> B.EventM Name (B.Next State)
handleEvent st (B.VtyEvent v@(Vty.EvKey k _)) = case k of
    Vty.KChar '\t' -> B.continue $ st & #focus %~ B.focusNext

    Vty.KEnter     -> B.continue $ st & #focus %~ B.focusSetCurrent ResultList

    Vty.KBackTab   -> B.continue $ st & #focus %~ B.focusPrev

    Vty.KBS        -> do
        r <- B.handleEditorEvent v $ st ^. #nameSearchEditor
        B.continue $ handleBackSpace st & #nameSearchEditor .~ r

    _              -> do
        r <- B.handleEditorEvent v $ st ^. #nameSearchEditor
        B.continue $ handleEntry st & #nameSearchEditor .~ r
handleEvent st _ = B.continue st

drawUI :: State -> B.Widget Name
drawUI st = hTitle "By name: "
    <+> editor SearchByName st (st ^. #nameSearchEditor)

footer :: [Text]
footer = [ "RET - Jump to results" ]
