{-# LANGUAGE OverloadedLabels #-}

module UI.Utils where

import qualified Brick              as B
import qualified Brick.Focus        as B
import qualified Brick.Widgets.Edit as B
import qualified Brick.Widgets.List as B

import           Data.Foldable      ( Foldable(foldl') )
import           Data.Text          ( Text )
import qualified Data.Text          as T

import           Lens.Micro

import           UI.Types           ( Name, State )

hTitle :: Text -> B.Widget n
hTitle = B.hLimit 20 . B.withAttr titleAttr . B.txt

editor :: Name -> State -> B.Editor Text Name -> B.Widget Name
editor n s ed =
    B.vLimit 1 $ B.renderEditor (B.txt . T.unlines)
                                (B.focusGetCurrent (s ^. #focus) == Just n)
                                ed

processSearch :: B.Editor Text n -> Text
processSearch = T.strip . T.concat . B.getEditContents

brickList :: Foldable t => n -> t a -> B.GenericList n t a
brickList n xs = B.list n xs 1

concatDigits :: (Foldable t, Integral i) => t i -> i
concatDigits = foldl' (\a b -> 10 * a + b) 0

titleAttr :: B.AttrName
titleAttr = "titleAttr"

dimAttr :: B.AttrName
dimAttr = "dimAttr"

footerAttr :: B.AttrName
footerAttr = "helpAttr"

footerKeyAttr :: B.AttrName
footerKeyAttr = "footerKeAttr"
