# ucpicker

`ucpicker` is a command-line tool, written in Haskell, to interactively search for and select Unicode characters by character name or codepoint.

![](./img/screenshot.png)

## Usage

`ucpicker` recognizes two commands:

    $ ucpicker download

fetches, parses, and caches the [ Unicode Character Database ](https://unicode.org/Public/UCD/latest/ucd/UnicodeData.txt).

    $ ucpicker pick

launches the interactive TUI to search for and select characters, performing the same actions as the above command if no cache is found.

## TODO

- Add more options for downloading into different formats.
- Parse more properties for each character (bidi, etc...).
- Add documentation
